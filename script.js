$(document).ready(function () {
  const base_url = "gatesbooks-api.uw.r.appspot.com/";
  const endpoint = "/observations/latest";

  $("#getwx").on("click", function (e) {
    var mystation = $("input#station-id").val();
    var myurl = base_url + mystation + endpoint;
    $("input#my-url").val(myurl);

    $("ul li").each(function () {
      $("ul").empty();
    });

    console.log("Cleared Elements of UL");
    //hfjkdslhfajkfhdjkfakdhfjkedhfakhdafhadjkfhal
    $.ajax({
      url: myurl,
      dataType: "json",
      success: function (data) {
        console.log(data.properties);
        var tempC_data = data.properties.temperature.value;
        var tempF = ((tempC_data * 9) / 5 + 32).toFixed(1);
        if (tempC_data !== null) {
          let tempC = tempC_data.toFixed(1);


          var myJSON = JSON.stringify(data);
          $("textarea").val(myJSON);

          var strTemp =
            "<li>Current temperature and conditions: " +
            tempC +
            "C / " +
            tempF +
            "F. " +
            weaInten +
            " outside.</li>";
          $("ul").append(strTemp);
          $("ul li:last").attr("class", "list-group-item");

          var strWind =
            "<li>Current winds: " +
            windSpeed +
            "km/h blowing in the " +
            windDirec +
            " degree direction." +
            "</li>";
          $("ul").append(strWind);
          $("ul li:last").attr("class", "list-group-item");

         

          $("ul li:last").attr("class", "list-group-item"); // add additional code here for the Wind direction, speed, weather contitions and icon image
        } else {
          console.log("Null temperature! Blyat!");
          var nullMessage =
            "Temperature from requested station ID returned as null! ";
          $("ul").append(nullMessage);
          $("ul li:last").attr("class", "list-group-item");
        }
      }
    });
  });
});